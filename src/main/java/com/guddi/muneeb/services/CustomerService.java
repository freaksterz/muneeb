package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.CustomerDao;
import com.guddi.muneeb.model.Customer;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.annotations.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by freaksterz on 27/08/16.
 */
@Service
@Configurable
public class CustomerService {

    @Autowired
    CustomerDao customerDao;

    public Customer getCustomer(String custId) {

        Customer customer = customerDao.findOne("customerId", custId);

        return customer;
    }

    public Customer getCustomerBy(String field, long value){

        Customer customer = customerDao.findOne(field, value);
        return  customer;
    }


    public Key<Customer> createCustomer(Customer customer) {

         Key<Customer> custId = customerDao.save(customer);
        return custId;

    }

    public List<Customer> getCustomers() {

        return customerDao.findAll();

    }



    public Customer updateCustomer(Customer customer)
    {
        customerDao.save(customer);
        return customer;
    }

    //TODO : check single delete of the document
    public WriteResult deleteCustomerById(String id) {

         return customerDao.deleteCustomerByField("customerId", id);

    }

    public long countCustomers(){
        return customerDao.count();
    }
}
