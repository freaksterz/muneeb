package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.CustomerCreditDao;
import com.guddi.muneeb.model.CustomerCredit;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 11/7/17.
 */

@Service
@Configurable
public class CustomerCreditService {
    @Autowired
    CustomerCreditDao customerCreditDao;

    public CustomerCredit getCustomerCredit(String custId) {

        CustomerCredit customerCredit = customerCreditDao.findOne("customerId", custId);

        return customerCredit;
    }

    public CustomerCredit getCustomerCreditBy(String field, long value){

        CustomerCredit customer = customerCreditDao.findOne(field, value);
        return  customer;
    }


    public Key<CustomerCredit> createCustomerCredit(CustomerCredit customerCredit) {

        Key<CustomerCredit> custCreditId = customerCreditDao.save(customerCredit);
        return custCreditId;

    }

    public List<CustomerCredit> getCustomersCredit() {

        return customerCreditDao.findAll();

    }



    public CustomerCredit updateCustomerCredit(CustomerCredit customerCredit)
    {
        customerCreditDao.save(customerCredit);
        return customerCredit;
    }

    //TODO : check single delete of the document
    public WriteResult deleteCustomerCreditById(String id) {

        return customerCreditDao.deleteCustomerCreditByField("customerId", id);

    }

    public long countCustomersCredit(){
        return customerCreditDao.count();
    }
}

