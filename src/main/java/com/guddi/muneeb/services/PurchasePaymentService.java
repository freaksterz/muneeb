package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.PurchasePaymentDao;
import com.guddi.muneeb.model.PurchasePayment;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 20/08/17.
 */

@Service
@Configurable
public class PurchasePaymentService {

    @Autowired(required=true)
    PurchasePaymentDao purchasePaymentDao;

    public PurchasePayment getPurchasePayment(String paymentId) {
        PurchasePayment SalesPayment = purchasePaymentDao.findOne("paymentId", paymentId);
        return SalesPayment;
    }

    public PurchasePayment getPurchasePaymentBy(String field, long value){
        PurchasePayment SalesPayment = purchasePaymentDao.findOne(field, value);
        return  SalesPayment;
    }

    public Key<PurchasePayment> createPurchasePayment(PurchasePayment purchasePayment) {
        Key<PurchasePayment> purchasePaymentId = purchasePaymentDao.save(purchasePayment);
        return purchasePaymentId;
    }

    public List<PurchasePayment> getPurchasePayment() {
        return purchasePaymentDao.findAll();
    }

    public PurchasePayment updatePurchasePayment(PurchasePayment purchasePayment)
    {
        purchasePaymentDao.save(purchasePayment);
        return purchasePayment;
    }

    public WriteResult deletePurchasePaymentId(String id) {

        return purchasePaymentDao.deletePurchasePaymentByField("paymentId", id);

    }

    public long countPurchasePayment(){
        return purchasePaymentDao.count();
    }
}
