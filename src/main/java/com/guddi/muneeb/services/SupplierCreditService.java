package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.SupplierCreditDao;
import com.guddi.muneeb.model.SupplierCredit;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */
@Service
public class SupplierCreditService {

    @Autowired
    SupplierCreditDao supplierCreditDao;


    public SupplierCredit getSupplierCredit(String suppliedId){
        SupplierCredit supplierCredit = supplierCreditDao.findOne("supplierId", suppliedId);
        return supplierCredit;
    }

    public SupplierCredit getSupplierCreditBy(String field, Long value){
        SupplierCredit supplierCredit = supplierCreditDao.findOne(field,value);
        return supplierCredit;
    }

    public List<SupplierCredit> getSupplierCredits() {
        return supplierCreditDao.findAll();
    }

    public SupplierCredit updateSupplierCredit( SupplierCredit supplierCredit){
        supplierCreditDao.save(supplierCredit);
        return supplierCredit;
    }

    public Key<SupplierCredit> createSupplierCredit (SupplierCredit supplierCredit){
        return  supplierCreditDao.save(supplierCredit);
    }


    public long countSuppliersCredit(){
        return supplierCreditDao.count();
    }


    public WriteResult deleteSupplierCreditById(String id) {
        return supplierCreditDao.deleteSupplierCreditByField("supplierId",id);
    }
}