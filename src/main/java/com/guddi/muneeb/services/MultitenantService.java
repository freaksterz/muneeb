package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.MultiDAO;
import com.guddi.muneeb.model.Customer;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by aman on 19/8/17.
 */
@Service
public class MultitenantService {

    @Autowired
    MultiDAO multiDAO;


    public Key<Customer> createCustomerMulti(Customer customer) {



        Key<Customer> customerKey = multiDAO.createCustomer(customer);

        return customerKey;
    }
}
