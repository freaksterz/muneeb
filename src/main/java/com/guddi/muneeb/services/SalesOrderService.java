package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.SalesOrderDao;
import com.guddi.muneeb.model.SalesOrder;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@Service
public class SalesOrderService {
    
    @Autowired
    SalesOrderDao salesOrderDao;

    public SalesOrder getSalesOrder(String suppliedId){
        SalesOrder salesOrder = salesOrderDao.findOne("saleOrderId", suppliedId);
        return salesOrder;
    }

    public SalesOrder getSalesOrderBy(String field, String value){
        SalesOrder salesOrder = salesOrderDao.findOne(field,value);
        return salesOrder;
    }


    public List<SalesOrder> getSalesOrderListByField(String field, String value ) {

        return salesOrderDao.getSaleOrderListByField(field,value);

    }
    public List<SalesOrder>getSalesOrders() {
        return salesOrderDao.findAll();
    }

    public SalesOrder updateSalesOrder(SalesOrder salesOrder){
        salesOrderDao.save(salesOrder);
        return salesOrder;
    }

    public Key<SalesOrder> createSalesOrder (SalesOrder salesOrder){
        return  salesOrderDao.save(salesOrder);
    }


    public long countSalesOrders(){
        return salesOrderDao.count();
    }


    public WriteResult deleteSalesOrderById(String id) {
        return deleteSalesOrderById(id);
    }


}
