package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.ItemDao;
import com.guddi.muneeb.model.Item;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freakster on 2/11/16.
 */
@Service
@Configurable
public class ItemService {

    @Autowired
    ItemDao itemDao;


    public Item getItem(String itemCode) {

        Item item = itemDao.findOne("itemCode", itemCode);
        return item;

    }

    public Key<Item> createItem(Item item){
        Key<Item> itemKey = itemDao.save(item);

        return itemKey;
    }


    public List<Item> getItems(){
        return itemDao.findAll();
    }

     public Item udpateItem(Item item){
         itemDao.save(item);
         return item;
     }

     public WriteResult deleteItemByCode(String itemCode){
         return itemDao.deleteItemByField("itemCode",itemCode);
     }








}
