package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.TenantDao;
import com.guddi.muneeb.model.Tenant;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freaksterz on 11/7/17.
 */
@Service
public class TenantService {

    @Autowired
    TenantDao TenantDao;


    public Tenant getTenant(String tenantId){
        Tenant Tenant = TenantDao.findOne("TenantId", tenantId);
        return Tenant;
    }

    public Tenant getTenantBy(String field, Long value){
        Tenant Tenant = TenantDao.findOne(field,value);
        return Tenant;
    }

    public List<Tenant> getTenants() {
        return TenantDao.findAll();
    }

    public Tenant updateTenant( Tenant Tenant){
        TenantDao.save(Tenant);
        return Tenant;
    }

    public Key<Tenant> createTenant (Tenant Tenant){
        //TODO : create a db in mongo instance with tenantId
        //TODO : Also add an entry in tenant : db combinations




        return  TenantDao.save(Tenant);
    }


    public long countTenants(){
        return TenantDao.count();
    }


    public WriteResult deleteTenantById(String id) {
        return TenantDao.deleteTenantByField("TenantId",id);
    }
}