package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.StockDao;
import com.guddi.muneeb.dao.StockSiteDao;
import com.guddi.muneeb.model.Stock;
import com.guddi.muneeb.model.StockSite;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Created by rishu on 12/7/17.
 */
@Service
@Configurable
public class StockService {

    @Autowired
    StockDao stockDao;

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

    public Stock getStockBy(String field, String value){
        Stock stock = stockDao.findOne(field, value);
        return stock;
    }

    public Key<Stock> createStock(Stock stock){
        Key<Stock> stockKey = stockDao.save(stock);
        return stockKey;
    }

    public Key<Stock> updateStock(Stock stock){
        Key<Stock> stockKey = stockDao.save(stock);
        return stockKey;
    }

    public List<Stock> getStocks(){
        return stockDao.findAll();
    }

    public WriteResult deleteStockby( String field, String value){

        return stockDao.deleteStockByField(field,value);
    }

}