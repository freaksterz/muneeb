package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.SupplierDao;
import com.guddi.muneeb.model.Supplier;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freaksterz on 8/11/16.
 */

@Service
public class SupplierService {

    @Autowired
    SupplierDao supplierDao;


    public Supplier getSupplier(String suppliedId){
        Supplier supplier = supplierDao.findOne("supplierId", suppliedId);
        return supplier;
    }

    public Supplier getSupplierBy(String field, Long value){
        Supplier supplier = supplierDao.findOne(field,value);
        return supplier;
    }

    public List<Supplier> getSuppliers() {
        return supplierDao.findAll();
    }

    public Supplier updateSupplier( Supplier supplier){
        supplierDao.save(supplier);
        return supplier;
    }

    public Key<Supplier> createSupplier (Supplier supplier){
        return  supplierDao.save(supplier);
    }


    public long countSuppliers(){
        return supplierDao.count();
    }


    public WriteResult deleteSupplierById(String id) {
        return supplierDao.deleteSupplierByField("supplierId",id);
    }
}