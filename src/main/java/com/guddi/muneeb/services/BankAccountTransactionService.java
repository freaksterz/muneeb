package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.BankAccountDao;
import com.guddi.muneeb.dao.BankAccountTransactionDao;
import com.guddi.muneeb.model.BankAccount;
import com.guddi.muneeb.model.BankAccountTransaction;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 11/7/17.
 */

@Service
@Configurable
public class BankAccountTransactionService {

    @Autowired
    BankAccountTransactionDao bankAccountTransactionDao;

    public BankAccountTransaction getBankAccountTransaction(String bankAccountTransactionId) {

        BankAccountTransaction bankAccountTransaction = bankAccountTransactionDao.findOne("transactionId", bankAccountTransactionId);
        return bankAccountTransaction;
    }

    public Key<BankAccountTransaction> createBankAccountTransaction(BankAccountTransaction bankAccountTransaction) {

        Key<BankAccountTransaction> bankid = bankAccountTransactionDao.save(bankAccountTransaction);
        return bankid;

    }

    public List<BankAccountTransaction> getBankAccountTransaction() {

        return bankAccountTransactionDao.findAll();

    }

    public BankAccountTransaction updateBankAccountTransaction(BankAccountTransaction bankAccountTransaction)
    {
        bankAccountTransactionDao.save(bankAccountTransaction);
        return bankAccountTransaction;
    }

    //TODO : check single delete of the document
    public WriteResult deleteBankAccountTransaction(String id) {

        return bankAccountTransactionDao.deleteBankAccountTransactionByField("transactionId", id);

    }

    public long countBankAccountTransaction(){
        return bankAccountTransactionDao.count();
    }
}
