package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.UserDao;
import com.guddi.muneeb.model.SecUserDetails;
import com.guddi.muneeb.model.User;
import jdk.nashorn.internal.runtime.options.Option;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by freakster on 8/10/16.
 */

@Transactional
@Service
public class SecUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        try {


            User user= userDao.findOne("username", username);

            if (user == null) {
                throw new UsernameNotFoundException(username);
            }

            return new UserDetailsDecorator(user);



            /*List<GrantedAuthority> authorities = new ArrayList<>();
            for (String role : user.getRoles()) {
                GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role);
                authorities.add(grantedAuthority);
            }
*/
            /*UserDetails userDetails = new org.springframework.security.core.userdetails.
                    User(user.getUsername(), user.getPassword(), authorities);*/

            //LOGGER.debug(" user from username " + user.toString());
           // UserDetails details = new SecUserDetails(user);
            //System.out.println("details = " + details.toString());
            /*return userDetails;*/

        } catch (Exception e) {
            throw new UsernameNotFoundException("User not found");
        }



    }

    private Set<GrantedAuthority> getAuthorities(User user) {

        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        for (String role : user.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role);
            authorities.add(grantedAuthority);
        }
        //LOGGER.debug("user authorities are " + authorities.toString());
        return authorities;

    }


    public User getUser(String userName) {

        User user = userDao.findOne("username", userName);
        return user;

    }

    public Key<User> createUser(User user) {

        if(user.getIsAccountNonExpired() == null)
            user.setAccountNonExpired(true);

        if(user.getIsAccountNonLocked() == null)
            user.setAccountNonLocked(true);

        if(user.getIsCredentialsNonExpired() == null)
            user.setCredentialsNonExpired(true);

        if(user.getIsEnabled() == null)
            user.setEnabled(true);

        Key<User> userKey = userDao.save(user);
        return userKey;
    }

    public List<User> getUsers() {
        return userDao.findAll();
    }

}

