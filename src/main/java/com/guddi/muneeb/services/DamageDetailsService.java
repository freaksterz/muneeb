package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.DamageDao;
import com.guddi.muneeb.dao.DamageDetailsDao;
import com.guddi.muneeb.model.Damage;
import com.guddi.muneeb.model.DamageDetails;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */
@Service
public class DamageDetailsService {

    @Autowired
    DamageDetailsDao damageDetailsDao;

    public DamageDetails damageDetailsById(String damageId){
        DamageDetails damageDetails = damageDetailsDao.findOne("damageId", damageId);
        return damageDetails;
    }



    public List<DamageDetails> damageDetails() {
        return damageDetailsDao.findAll();
    }

    public DamageDetails updateDamageDetails( DamageDetails damageDetails){
        damageDetailsDao.save(damageDetails);
        return damageDetails;

    }

    public Key<DamageDetails> createDamageDetails (DamageDetails damageDetails){
        return  damageDetailsDao.save(damageDetails);
    }


    public long countDamagesDetails(){
        return damageDetailsDao.count();
    }


    public WriteResult deleteDamageDetailsById(String id) {
        return deleteDamageDetailsById(id);
    }


}
