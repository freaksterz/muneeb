package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.PurchaseOrderDao;
import com.guddi.muneeb.model.PurchaseOrder;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@Service
@Configurable
public class PurchaseOrderService {

    @Autowired
    PurchaseOrderDao purchaseOrderDao;


    public PurchaseOrder getPurchaseOrder(String suppliedId){
        PurchaseOrder purchaseOrder = purchaseOrderDao.findOne("purchaseOrderId", suppliedId);
        return purchaseOrder;
    }

    public PurchaseOrder getPurchaseOrderBy(String field, String value){
        PurchaseOrder purchaseOrder = purchaseOrderDao.findOne(field,value);
        return purchaseOrder;
    }


    public List<PurchaseOrder> getPurchaseOrderListByField( String field, String value ) {

        return purchaseOrderDao.getPurchaseOrderListByField(field,value);

    }
    public List<PurchaseOrder>getPurchaseOrders() {
        return purchaseOrderDao.findAll();
    }

    public PurchaseOrder updatePurchaseOrder( PurchaseOrder purchaseOrder){
        purchaseOrderDao.save(purchaseOrder);
        return purchaseOrder;
    }

    public Key<PurchaseOrder> createPurchaseOrder (PurchaseOrder purchaseOrder){
        return  purchaseOrderDao.save(purchaseOrder);
    }


    public long countPurchaseOrders(){
        return purchaseOrderDao.count();
    }


    public WriteResult deletePurchaseOrderById(String id) {
        return deletePurchaseOrderById(id);
    }
    
    
    

}
