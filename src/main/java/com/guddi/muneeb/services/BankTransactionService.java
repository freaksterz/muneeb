package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.BankTransactionDao;
import com.guddi.muneeb.model.BankTransaction;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 11/7/17.
 */

@Service
@Configurable
public class BankTransactionService {

    @Autowired
    BankTransactionDao bankTransactionDao;

    public BankTransaction getBankTransaction(String bankTransactionId) {

        BankTransaction bankTransaction = bankTransactionDao.findOne("transactionId", bankTransactionId);
        return bankTransaction;
    }

    public Key<BankTransaction> createBankTransaction(BankTransaction bankTransaction) {

        Key<BankTransaction> bankTransactionId = bankTransactionDao.save(bankTransaction);
        return bankTransactionId;

    }

    public List<BankTransaction> getBankTransaction() {

        return bankTransactionDao.findAll();

    }

    public BankTransaction updateBankTransaction(BankTransaction bankTransaction)
    {
        bankTransactionDao.save(bankTransaction);
        return bankTransaction;
    }

    //TODO : check single delete of the document
    public WriteResult deleteBankTransaction(String id) {

        return bankTransactionDao.deleteBankTransactionByField("transactionId","id");

    }

    public long countBankTransaction(){
        return bankTransactionDao.count();
    }
}
