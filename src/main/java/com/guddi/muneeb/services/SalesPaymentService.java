package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.BankAccountDao;
import com.guddi.muneeb.dao.SalesPaymentDao;
import com.guddi.muneeb.model.BankAccount;
import com.guddi.muneeb.model.SalesPayment;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Created by rishu on 20/08/17.
 */

@Service
@Configurable
public class SalesPaymentService {

    @Autowired(required=true)
    SalesPaymentDao salesPaymentDao;

    public SalesPayment getSalesPayment(String customerId) {
        SalesPayment SalesPayment = salesPaymentDao.findOne("customerId", customerId);
        return SalesPayment;
    }

    public SalesPayment getSalesPaymentBy(String field, long value){
        SalesPayment SalesPayment = salesPaymentDao.findOne(field, value);
        return  SalesPayment;
    }

    public Key<SalesPayment> createSalesPayment(SalesPayment salesPayment) {
        Key<SalesPayment> salesPaymentId = salesPaymentDao.save(salesPayment);
        return salesPaymentId;
    }

    public List<SalesPayment> getSalesPayment() {
        return salesPaymentDao.findAll();
    }

    public SalesPayment updateSalesPayment(SalesPayment salesPayment)
    {
        salesPaymentDao.save(salesPayment);
        return salesPayment;
    }

    public WriteResult deleteSalesPaymentId(String id) {

        return salesPaymentDao.deleteSalesPaymentByField("customerId", id);

    }

    public long countSalesPayment(){
        return salesPaymentDao.count();
    }
}
