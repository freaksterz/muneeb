package com.guddi.muneeb.services;
import com.guddi.muneeb.dao.PurchaseReturnDao;
import com.guddi.muneeb.model.PurchaseReturn;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Created by rishu on 13/7/17.
 */
@Service
@Configurable
public class PurchaseReturnService {
    @Autowired
    PurchaseReturnDao purchaseReturnDao;


    public PurchaseReturn getPurchaseReturnById(String suppliedId){
        PurchaseReturn purchaseReturn = purchaseReturnDao.findOne("suppliedId", suppliedId);
        return purchaseReturn;
    }

    public PurchaseReturn getPurchaseReturnBy(String field, String value){
        PurchaseReturn purchaseReturn = purchaseReturnDao.findOne(field,value);
        return purchaseReturn;
    }


    public List<PurchaseReturn> getPurchaseReturnListByField( String field, String value ) {

        return purchaseReturnDao.getPurchaseReturnListByField(field,value);

    }
    public List<PurchaseReturn>  getPurchaseReturns() {
        return purchaseReturnDao.findAll();
    }

    public PurchaseReturn updatePurchaseReturn( PurchaseReturn purchaseReturn){
        purchaseReturnDao.save(purchaseReturn);
        return purchaseReturn;
    }

    public Key<PurchaseReturn> createPurchaseReturn(PurchaseReturn purchaseReturn){
        return  purchaseReturnDao.save(purchaseReturn);
    }


    public long countPurchaseReturn(){
        return purchaseReturnDao.count();
    }


    public WriteResult deletePurchaseReturnById(String id) {
        return deletePurchaseReturnById(id);
    }

}

