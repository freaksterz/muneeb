package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.ExpenseDao;
import com.guddi.muneeb.model.Expense;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@Service
public class ExpenseService {


    @Autowired
    ExpenseDao expenseDao;

    public Expense getExpense(String expenseId) {

        Expense expense =expenseDao.findOne("expenseId", expenseId);

        return expense;
    }

    public Expense getExpenseBy(String field, long value){

        Expense expense = expenseDao.findOne(field, value);
        return  expense;
    }


    public Key<Expense> createExpense(Expense expense) {

        Key<Expense> custId = expenseDao.save(expense);
        return custId;

    }

    public List<Expense> getExpenses() {

        return expenseDao.findAll();

    }



    public Expense updateExpense(Expense expense)
    {
        expenseDao.save(expense);
        return expense;
    }

    //TODO : check single delete of the document
    public WriteResult deleteExpenseById(String id) {

        return expenseDao.deleteExpenseByField("expenseId", id);

    }

    public long countExpenses(){
        return expenseDao.count();
    }




}
