package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.StockSiteDao;
import com.guddi.muneeb.model.StockSite;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freakster on 3/11/16.
 */
@Service
@Configurable
public class StockSiteService {

    @Autowired
    StockSiteDao stockSiteDao;

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

    public StockSite getStockSiteBy(String field, String value){
        StockSite stockSite = stockSiteDao.findOne(field, value);
        return stockSite;
    }

    public Key<StockSite> createStockSite(StockSite stockSite){
        Key<StockSite> stockSiteKey = stockSiteDao.save(stockSite);
        return stockSiteKey;
    }

    public Key<StockSite> updateStockSite(StockSite stockSite){
        Key<StockSite> stockSiteKey = stockSiteDao.save(stockSite);
        return stockSiteKey;
    }

    public List<StockSite> getStockSites(){
        return stockSiteDao.findAll();
    }

    public WriteResult deleteStockSiteby( String field, String value){

        return stockSiteDao.deleteStockSiteByField(field,value);

    }

}