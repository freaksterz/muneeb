package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.OrderDetailsDao;
import com.guddi.muneeb.model.OrderDetails;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */
@Service
@Configurable
public class OrderDetailsService {

    @Autowired
    OrderDetailsDao orderDetailsDao;


    public OrderDetails getOrderDetails(String orderId) {
        OrderDetails orderDetails = orderDetailsDao.findOne("orderId", orderId);
        return orderDetails;
    }

    public OrderDetails getOrderDetailsOrderBy(String field, String value) {
        OrderDetails orderDetails = orderDetailsDao.findOne(field, value);
        return orderDetails;
    }


    public List<OrderDetails> getOrderDetailsListByField(String field, String value) {

        return orderDetailsDao.getOrderDetailsListByField(field, value);

    }

    public List<OrderDetails> getOrderDetails() {
        return orderDetailsDao.findAll();
    }

    public OrderDetails updateOrderDetails(OrderDetails orderDetails) {
        orderDetailsDao.save(orderDetails);
        return orderDetails;
    }

    public Key<OrderDetails> createOrderDetails(OrderDetails orderDetails) {
        return orderDetailsDao.save(orderDetails);
    }

    public long countOrderDetails() {
        return orderDetailsDao.count();
    }

    public WriteResult deleteOrderDetailsById(String id) {
        return deleteOrderDetailsById(id);
    }
}
