package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.DamageDao;
import com.guddi.muneeb.model.Damage;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by freakster on 9/11/16.++
 */
@Service
public class DamageService {

    @Autowired
    DamageDao damageDao;

    public Damage getDamage(String suppliedId){
        Damage damage = damageDao.findOne("damageId", suppliedId);
        return damage;
    }

    public Damage getDamageBy(String field, Long value){
        Damage damage = damageDao.findOne(field,value);
        return damage;
    }

    public List<Damage> getDamages() {
        return damageDao.findAll();
    }

    public Damage updateDamage( Damage damage){
        damageDao.save(damage);
        return damage;

    }

    public Key<Damage> createDamage (Damage damage){
        return  damageDao.save(damage);
    }


    public long countDamages(){
        return damageDao.count();
    }


    public WriteResult deleteDamageById(String id) {
        return deleteDamageById(id);
    }
    
    
    
    
}
