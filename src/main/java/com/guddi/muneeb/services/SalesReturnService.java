package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.SalesOrderDao;
import com.guddi.muneeb.dao.SalesReturnDao;
import com.guddi.muneeb.model.SalesOrder;
import com.guddi.muneeb.model.SalesReturn;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rishu on 8/11/16.
 */
@Service
public class SalesReturnService {

    @Autowired
    SalesReturnDao salesReturnDao;

    public SalesReturn getSalesReturn(String suppliedId){
        SalesReturn salesReturn = salesReturnDao.findOne("saleOrderId", suppliedId);
        return salesReturn;
    }

    public SalesReturn getSalesReturnBy(String field, String value){
        SalesReturn salesReturn = salesReturnDao.findOne(field,value);
        return salesReturn;
    }


    public List<SalesReturn> getSalesReturnListByField(String field, String value ) {

        return salesReturnDao.getSaleReturnListByField(field,value);

    }
    public List<SalesReturn> getSalesReturn() {
        return salesReturnDao.findAll();
    }

    public SalesReturn updateSalesReturn(SalesReturn salesReturn){
        salesReturnDao.save(salesReturn);
        return salesReturn;
    }

    public Key<SalesReturn> createSalesReturn(SalesReturn salesReturn){
        return  salesReturnDao.save(salesReturn);
    }


    public long countSalesReturn(){
        return salesReturnDao.count();
    }


    public WriteResult deleteSalesReturnById(String id) {
        return deleteSalesReturnById(id);
    }


}
