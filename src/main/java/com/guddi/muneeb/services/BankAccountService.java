package com.guddi.muneeb.services;

import com.guddi.muneeb.dao.BankAccountDao;
import com.guddi.muneeb.model.BankAccount;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Created by rishu on 11/7/17.
 */

@Service
@Configurable
public class BankAccountService {

    @Autowired
    BankAccountDao bankAccountDao;

    public BankAccount getBankAccount(String bankAccountId) {

        BankAccount bankAccount = bankAccountDao.findOne("bankAccountId", bankAccountId);

        return bankAccount;
    }

    public BankAccount getBankAccountBy(String field, long value){

        BankAccount bankAccount = bankAccountDao.findOne(field, value);
        return  bankAccount;
    }


    public Key<BankAccount> createBankAccount(BankAccount bankAccount) {

        Key<BankAccount> bankid = bankAccountDao.save(bankAccount);
        return bankid;

    }

    public List<BankAccount> getBankAccount() {

        return bankAccountDao.findAll();

    }



    public BankAccount updateBankAccount(BankAccount bankAccount)
    {
        bankAccountDao.save(bankAccount);
        return bankAccount;
    }

    //TODO : check single delete of the document
    public WriteResult deleteBankAccountId(String id) {

        return bankAccountDao.deleteCustomerByField("customerId", id);

    }

    public long countBankAccount(){
        return bankAccountDao.count();
    }
}
