package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.SalesOrder;
import com.guddi.muneeb.model.SalesReturn;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rishu on 8/11/16.
 */
@Repository
public class SalesReturnDao extends AbstractGenericRepository<SalesReturn, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;


    public List findAll()
    {
        return super.getDatastore().find( SalesReturn.class ).asList();
    }


    public WriteResult deleteSalesReturnByField(String field, String value){
        Query<SalesReturn> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }


    public List<SalesReturn> getSaleReturnListByField(String field, String value) {

        Query<SalesReturn> query = datastore.createQuery(SalesReturn.class);

        List<SalesReturn> salesReturns = (List<SalesReturn>) query.criteria(field).containsIgnoreCase(value);

        return salesReturns;
    }


}
