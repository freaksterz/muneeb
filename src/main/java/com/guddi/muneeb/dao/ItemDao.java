package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.Item;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freakster on 2/11/16.
 */

@Repository
public class ItemDao extends AbstractGenericRepository<Item, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;



    public List<Item> findAll() { return  super.getDatastore().find( Item.class ).asList();}

    public WriteResult deleteItemByField(String field, String value){
        Query<Item> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }

}
