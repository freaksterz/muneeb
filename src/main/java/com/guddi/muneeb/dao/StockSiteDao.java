package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.StockSite;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freakster on 3/11/16.
 */
@Repository
public class StockSiteDao extends AbstractGenericRepository<StockSite, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

    public List findAll()
    {
        return super.getDatastore().find( StockSite.class ).asList();
    }


    public WriteResult deleteStockSiteByField(String field, String value){
        Query<StockSite> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }

}
