package com.guddi.muneeb.dao;


import com.guddi.muneeb.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freakster on 5/10/16.
 */
@Repository
public class UserDao extends AbstractGenericRepository<User,String> {

    public List findAll()
    {
        return super.getDatastore().find( User.class ).asList();
    }

}
