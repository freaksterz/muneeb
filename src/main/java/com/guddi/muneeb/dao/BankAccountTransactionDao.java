package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.BankAccountTransaction;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rishu on 11/7/17.
 */
@Repository
public class BankAccountTransactionDao extends AbstractGenericRepository<BankAccountTransaction,String> {
    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

    public List findAll()
    {
        return super.getDatastore().find(BankAccountTransaction.class).asList();
    }

    public WriteResult deleteBankAccountTransactionByField(String field, String value)
    {
        Query<BankAccountTransaction> query = createQuery();
        query.field(field).equals(value);
        return  deleteByQuery(query);
    }
}
