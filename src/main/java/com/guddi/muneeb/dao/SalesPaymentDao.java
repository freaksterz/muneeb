package com.guddi.muneeb.dao;


import com.guddi.muneeb.model.SalesPayment;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rishu on 20/8/17.
 */
@Repository
public class SalesPaymentDao extends AbstractGenericRepository<SalesPayment, String> {

    @Autowired(required = true)
    @Qualifier("datastore")
    Datastore datastore;


    public List findAll()
    {
        return super.getDatastore().find(SalesPayment.class ).asList();
    }

    public List<SalesPayment> getSalesPaymentByField(String field, String value) {

        Query<SalesPayment> query = datastore.createQuery(SalesPayment.class);

        List<SalesPayment> salesPaymentsReturns = (List<SalesPayment>) query.criteria(field).containsIgnoreCase(value);

        return salesPaymentsReturns;
    }

    public WriteResult deleteSalesPaymentByField(String field, String value)
    {
        Query<SalesPayment> query = createQuery();
        query.field(field).equals(value);
        return  deleteByQuery(query);
    }

}
