package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.Tenant;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freaksterz on 11/7/17.
 */


@Repository
public class TenantDao extends AbstractGenericRepository<Tenant,String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;


    public List findAll()
    {
        return super.getDatastore().find( Tenant.class ).asList();
    }


    public WriteResult deleteTenantByField(String field, String value){
        Query<Tenant> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }

}
