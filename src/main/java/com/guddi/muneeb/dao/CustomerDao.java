package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.Customer;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freaksterz on 28/08/16.
 */
@Repository
public class CustomerDao extends AbstractGenericRepository<Customer,String >{

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;


    public List findAll()
    {
        return super.getDatastore().find( Customer.class ).asList();
    }

    public WriteResult deleteCustomerByField( String field, String value){
        Query<Customer> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);
    }



}
