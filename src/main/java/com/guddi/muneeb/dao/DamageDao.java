package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.Damage;
import com.guddi.muneeb.model.Supplier;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@Repository
public class DamageDao extends AbstractGenericRepository<Damage, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

       public List findAll()
    {
        return super.getDatastore().find( Damage.class ).asList();
    }


    public WriteResult deleteSupplierByField(String field, String value){
        Query<Damage> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }
    
}
