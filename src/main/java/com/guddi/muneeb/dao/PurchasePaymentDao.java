package com.guddi.muneeb.dao;


import com.guddi.muneeb.model.PurchasePayment;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rishu on 20/8/17.
 */
@Repository
public class PurchasePaymentDao extends AbstractGenericRepository<PurchasePayment, String> {

    @Autowired(required = true)
    @Qualifier("datastore")
    Datastore datastore;

      public List findAll()
    {
        return super.getDatastore().find(PurchasePayment.class ).asList();
    }

    public List<PurchasePayment> getPurchasePaymentByField(String field, String value) {

        Query<PurchasePayment> query = datastore.createQuery(PurchasePayment.class);

        List<PurchasePayment> purchaePaymentReturns = (List<PurchasePayment>) query.criteria(field).containsIgnoreCase(value);

        return purchaePaymentReturns;
    }

    public WriteResult deletePurchasePaymentByField(String field, String value)
    {
        Query<PurchasePayment> query = createQuery();
        query.field(field).equals(value);
        return  deleteByQuery(query);
    }

}
