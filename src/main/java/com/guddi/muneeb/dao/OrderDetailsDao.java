package com.guddi.muneeb.dao;
import com.guddi.muneeb.model.OrderDetails;
import com.guddi.muneeb.model.PurchaseOrder;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Created by rishu on 12/7/17.
 */
@Repository
public class OrderDetailsDao  extends AbstractGenericRepository<OrderDetails, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

      public List findAll()
    {
        return super.getDatastore().find( OrderDetails.class ).asList();
    }


    public WriteResult deleteOrderDetailsByField(String field, String value){
        Query<OrderDetails> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }

    public List<OrderDetails> getOrderDetailsListByField(String field, String value) {

        Query<OrderDetails> query = datastore.createQuery(OrderDetails.class);
        List<OrderDetails> orderDetials = (List<OrderDetails>) query.criteria(field).containsIgnoreCase(value);
        return orderDetials;
    }
}
