package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.Stock;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Created by rishu on 12/7/17.
 */
@Repository
public class StockDao extends AbstractGenericRepository<Stock, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;


    public List findAll()
    {
        return super.getDatastore().find( Stock.class ).asList();
    }

    public WriteResult deleteStockByField(String field, String value){
        Query<Stock> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);
    }
}

