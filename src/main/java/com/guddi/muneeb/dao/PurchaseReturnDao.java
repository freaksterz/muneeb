package com.guddi.muneeb.dao;


import com.guddi.muneeb.model.PurchaseOrder;
import com.guddi.muneeb.model.PurchaseReturn;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rishu on 13/7/17.
 */
@Repository
public class PurchaseReturnDao extends AbstractGenericRepository<PurchaseReturn, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

    public List findAll()
    {
        return super.getDatastore().find( PurchaseReturn.class ).asList();
    }


    public WriteResult deletePurchaseOrderByField(String field, String value){
        Query<PurchaseReturn> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }


    public List<PurchaseReturn> getPurchaseReturnListByField(String field, String value) {

        Query<PurchaseReturn> query = datastore.createQuery(PurchaseReturn.class);

        List<PurchaseReturn> purchaseReturns = (List<PurchaseReturn>) query.criteria(field).containsIgnoreCase(value);

        return purchaseReturns;
    }
}
