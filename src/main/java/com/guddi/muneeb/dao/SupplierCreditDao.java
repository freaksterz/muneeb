package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.Supplier;
import com.guddi.muneeb.model.SupplierCredit;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */
@Repository
public class SupplierCreditDao  extends AbstractGenericRepository<SupplierCredit,String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

     public List findAll() {
        return super.getDatastore().find(SupplierCredit.class).asList();
    }

    public WriteResult deleteSupplierCreditByField(String field, String value) {
        Query<SupplierCredit> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }
}
