package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.PurchaseOrder;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@Repository
public class PurchaseOrderDao extends AbstractGenericRepository<PurchaseOrder, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;

    public List findAll()
    {
        return super.getDatastore().find( PurchaseOrder.class ).asList();
    }


    public WriteResult deletePurchaseOrderByField(String field, String value){
        Query<PurchaseOrder> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }


    public List<PurchaseOrder> getPurchaseOrderListByField(String field, String value) {

        Query<PurchaseOrder> query = datastore.createQuery(PurchaseOrder.class);

        List<PurchaseOrder> purchaseOrders = (List<PurchaseOrder>) query.criteria(field).containsIgnoreCase(value);

        return purchaseOrders;
    }
}
