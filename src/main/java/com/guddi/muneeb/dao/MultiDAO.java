package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.Customer;
import org.mongodb.morphia.Key;
import org.springframework.stereotype.Repository;

/**
 * Created by aman on 19/8/17.
 */

@Repository
public class MultiDAO extends AbstractGenericRepository<Customer, String> {

    public Key<Customer> createCustomer(Customer customer){

        Key<Customer> customerKey = super.save(customer);

        return customerKey;

    }

}
