package com.guddi.muneeb.dao;

import com.guddi.muneeb.model.SalesOrder;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@Repository
public class SalesOrderDao extends AbstractGenericRepository<SalesOrder, String> {

    @Autowired
    @Qualifier("datastore")
    Datastore datastore;



    public List findAll()
    {
        return super.getDatastore().find( SalesOrder.class ).asList();
    }


    public WriteResult deleteSalesOrderByField(String field, String value){
        Query<SalesOrder> query = createQuery();
        query.field(field).equals(value);
        return deleteByQuery(query);

    }

    public List<SalesOrder> getSaleOrderListByField(String field, String value) {

        Query<SalesOrder> query = datastore.createQuery(SalesOrder.class);

        List<SalesOrder> salesOrders = (List<SalesOrder>) query.criteria(field).containsIgnoreCase(value);
        
        return salesOrders;
    }


}
