package com.guddi.muneeb.config.tenant;

/**
 * Created by freaksterz on 13/7/17.
 */
public class TenantContext {

    final public static String DEFAULT_TENANT = "master";

    private static final ThreadLocal<MongoConnectionConfig> CURRENT_CONFIG = new InheritableThreadLocal<>();


    public static void setCurrentTenant(String tenant) {
        currentTenant.set(tenant);
    }

    public static String getCurrentTenant() {
        return currentTenant.get();
    }

    public static void clear() {
        currentTenant.remove();
    }

}