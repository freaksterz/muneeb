package com.guddi.muneeb.config.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by freaksterz on 29/7/17.
 * for connecting to shell
 * mongo ds115613.mlab.com:15613/bahi -u admin -p admin123
 *
 */
@Configuration
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@EnableMongoRepositories("com.guddi.muneeb.model")
@Slf4j
public class MorphiaAutoConfiguration {

    @Value("${mongodb.uri}")
    private String uri;

    @Value("${mongodb.dbName}")
    private String defaultDBName;

    @Value("${mongodb.base.package}")
    private String basePackage;

    public MorphiaAutoConfiguration() {
    }

    @Bean
    public MongoClient mongoClient() {
        return createMongoClient();
    }

    public MongoClient createMongoClient() {
        //String dbURI = "mongodb://admin:admin123@ds115613.mlab.com:15613/bahi";
        //String uri = "mongodb://localhost";
        MongoClient mongoClient = new MongoClient(new MongoClientURI(uri));
        mongoClient.setWriteConcern();
        //System.out.println(mongoClient.getCredentialsList());
        return mongoClient;
    }

    @Bean
    public Datastore datastore() throws Exception {
        return createDataStore(mongoClient(), defaultDBName, Boolean.TRUE);
    }

    public Datastore createDataStore(MongoClient mongoClient, String dbName,
                                     boolean indexRequired) {
        Morphia morphia = new Morphia();
        ClassPathScanningCandidateComponentProvider entityScanner = new ClassPathScanningCandidateComponentProvider(
                true);
        entityScanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));
        for (BeanDefinition candidate : entityScanner
                .findCandidateComponents(basePackage)) {
            try {
                morphia.map(Class.forName(candidate.getBeanClassName()));
            } catch (ClassNotFoundException ex) {
                log.info("Unable to load the class:" + ex.getMessage());
            }
        }
        Datastore datastore = morphia.createDatastore(mongoClient, dbName);
        if (indexRequired) {
            datastore.ensureIndexes();
        }
        return datastore;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();

    }

    public String getDefaultDBName() {
        return defaultDBName;
    }

}
