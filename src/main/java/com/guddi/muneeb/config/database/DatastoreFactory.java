package com.guddi.muneeb.config.database;

import org.mongodb.morphia.Datastore;

/**
 * Created by freaksterz on 29/7/17.
 */
public interface DatastoreFactory {
    public Datastore getDS();
}
