package com.guddi.muneeb.config.database;

/**
 * Created by freaksterz on 30/7/17.
 */

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.mongodb.morphia.dao.DAO;
import org.slf4j.MDC;
import org.springframework.aop.framework.ReflectiveMethodInvocation;

@Slf4j
public class MongoMultiTenancyInterceptor implements MethodInterceptor {

    public Object invoke(MethodInvocation invocation) throws Throwable {

        ReflectiveMethodInvocation reflectiveMethodInvocation = (ReflectiveMethodInvocation) invocation;

        Object target = reflectiveMethodInvocation.getThis();

        if (target instanceof DAO) {

            String tenantDBName = MDC.get("mongodb");

            log.info("Switching to database:  "+tenantDBName);

            if(Strings.isNullOrEmpty(tenantDBName)){
                MultitenantDatastoreFactory.setDatabaseNameForCurrentThread(tenantDBName);
            }

        }
        Object object= invocation.proceed();

        MultitenantDatastoreFactory.clearDatabaseNameForCurrentThread();

        return object;

    }

}