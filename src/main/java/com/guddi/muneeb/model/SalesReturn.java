package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;

/**
 * Created by freakster on 1/11/16.
 */
@Getter
@Setter
@Entity( noClassnameStored = false)
public class SalesReturn extends BaseEntity {

    private String customerId;
    private String customerName;
    private String returnOverOrderId;

    private OrderDetails orderDetails;

    private String returnUserId;
    private String note;
    private String returnReason;

    @Override
    public String toString() {
        return "SalesReturn{" +
                "customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", returnOverOrderId='" + returnOverOrderId + '\'' +
                ", orderDetails=" + orderDetails +
                ", returnUserId='" + returnUserId + '\'' +
                ", comments='" + note + '\'' +
                ", returnReason='" + returnReason + '\'' +
                '}';
    }
}
