package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.PreSave;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by freakster on 5/10/16.
 */

@Entity( noClassnameStored = true )
public class User extends BaseEntity implements Serializable{


    private static final long serialVersionUID = 2140904871643897249L;
    private String username; // username
    private String name; // full name
    private String password; //TODO: should be hashed and encrypted
    private String tenantId;
    private String[] roles;
    private Boolean isEnabled;
    private Boolean isAccountNonExpired;
    private Boolean isAccountNonLocked;
    private Boolean isCredentialsNonExpired;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public User() {

    }

    /*@PreSave
    void preSave(User user){
        if(user.isAccountNonExpired == null)
            user.isAccountNonExpired = true;

        if(user.isAccountNonLocked == null)
            user.isAccountNonLocked = true;

        if(user.isCredentialsNonExpired == null)
            user.isCredentialsNonExpired = true;


        if(user.isEnabled == null)
            user.isEnabled = true;

    }*/



    public String getUserName() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public Boolean getIsAccountNonExpired() {
        return isAccountNonExpired;
    }

    public void setAccountNonExpired(Boolean accountNonExpired) {
        isAccountNonExpired = accountNonExpired;
    }

    public Boolean getIsAccountNonLocked() {
        return isAccountNonLocked;
    }

    public void setAccountNonLocked(Boolean accountNonLocked) {
        isAccountNonLocked = accountNonLocked;
    }

    public Boolean getIsCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
        isCredentialsNonExpired = credentialsNonExpired;
    }

    // username works as ID here, if there is normal ID column, use that of course
    // equals/hashCode is very important for AuthenticationServiceImpl#tokens to work properly
    @Override
    public boolean equals(Object o) {
        return this == o
                || o != null && o instanceof User
                && Objects.equals(username, ((User) o).username);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username);
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", name='" + name +
                '}';
    }
}
