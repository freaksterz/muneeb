package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by freakster on 1/11/16.
 */
@Getter
@Setter
@Entity( noClassnameStored = true )
public class PurchaseReturn extends BaseEntity {

     private String supplierId;
     private String supplierName;
     private String returnOverOrderId;

     private OrderDetails orderDetails;

     private String returnUserId;
     private String note;
     private String returnReason;

     @Override
     public String toString() {
          return "PurchaseReturn{" +
                  "supplierId='" + supplierId + '\'' +
                  ", supplierName='" + supplierName + '\'' +
                  ", returnOverOrderId='" + returnOverOrderId + '\'' +
                  ", orderDetails=" + orderDetails +
                  ", returnUserId='" + returnUserId + '\'' +
                  ", note='" + note + '\'' +
                  ", returnReason='" + returnReason + '\'' +
                  '}';
     }
}
