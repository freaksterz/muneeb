package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by freakster on 29/10/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true)
public class SalesOrder extends BaseEntity{

    private String saleOrderId;
    private String customerId;
    private String customerName;
    private Date dateOfPurchase;
    private BigDecimal discount;
    private BigDecimal totalPrice;
    private String author;

    @Embedded
    private OrderDetails orderDetails;

    @Override
    public String toString() {
        return "SaleOrder{" +
                "saleOrderId='" + saleOrderId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", dateOfPurchase=" + dateOfPurchase +
                ", discount=" + discount +
                ", totalPrice=" + totalPrice +
                ", author='" + author + '\'' +
                ", orderDetails=" + orderDetails +
                '}';
    }
}
