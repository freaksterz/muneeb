package com.guddi.muneeb.model;


import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.math.BigInteger;
import java.util.Date;
/**
 * Created by rishu on 20/8/17.
 */
@Setter
@Getter
@Entity( noClassnameStored = true)
public class SalesPayment extends BaseEntity {

    private String customerId;
    private String salesPaymentId;
    private boolean isGenericPayment;
    private boolean isOrderPayement;
    private String salesOrderId;
    private BigInteger amount;
    private String userCollectedId;
    private Date paymentDate;

    @Override
    public String toString() {
        return "SalesPayment{" +
                "customerId='" + customerId + '\'' +
                ", salesPaymentId='" + salesPaymentId + '\'' +
                ", isGenericPayment='" + isGenericPayment + '\'' +
                ", isOrderPayement=" + isOrderPayement +
                ", salesOrderId='" + salesOrderId + '\'' +
                ", amount='" + amount + '\'' +
                ", userCollectedId='" + userCollectedId + '\'' +
                ", paymentDate='" + paymentDate + '\'' +
                '}';
    }
}
