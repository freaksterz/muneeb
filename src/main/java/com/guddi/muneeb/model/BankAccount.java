package com.guddi.muneeb.model;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by freaksterz on 29/08/16.
 * This domain class stores the Bank Account information
 */
@Entity( noClassnameStored=true )
public class BankAccount extends BaseEntity {

    private String customerId;
    private String supplierId;
    private String accSerial;
    private BigDecimal currBalance;
    private String name;
    private BigInteger accNumber;
    private String ifscCode;
    private String city;
    private String holderName;

    @Override
    public String toString() {
        return "BankAccount{" +
                "customerId='" + customerId + '\'' +
                ", supplierId='" + supplierId + '\'' +
                ", accSerial='" + accSerial + '\'' +
                ", currBalance=" + currBalance +
                ", name='" + name + '\'' +
                ", accNumber='" + accNumber + '\'' +
                ", ifscCode='" + ifscCode + '\'' +
                ", city='" + city + '\'' +
                ", holderName='" + holderName + '\'' +
                '}';
    }

    // TODO : implement bank account details for user and bank account info
    //@Embedded
    //private BankAccountTransaction bankAccountTransaction;

}
