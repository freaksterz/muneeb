package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Reference;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by freaksterz on 27/08/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true )
public class Supplier extends BaseEntity {

    private String supplierId;
    private String name;

    @Embedded
    private Address address;

    private long phoneNumber;
    private long secondaryPhoneNumber;

    @Reference
    private List<BankAccount> bankAccountList;

    private String email;

    private String region;   // convert it to ENUM

    @Override
    public String toString() {
        return "Supplier{" +
                "supplierId='" + supplierId + '\'' +
                ", name='" + name + '\'' +
                ", address=" + address +
                ", phoneNumber=" + phoneNumber +
                ", secondaryPhoneNumber=" + secondaryPhoneNumber +
                ", bankAccountList=" + bankAccountList +
                ", email='" + email + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}