package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;

/**
 * Created by freakster on 29/10/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true )
public class StockSite extends BaseEntity {

    String regionName;   //TODO : convert to ENUM
    String siteName;
    String siteCode;

    @Override
    public String toString() {
        return "StockSite{" +
                "regionName='" + regionName + '\'' +
                ", siteName='" + siteName + '\'' +
                ", siteCode='" + siteCode + '\'' +
                '}';
    }
}
