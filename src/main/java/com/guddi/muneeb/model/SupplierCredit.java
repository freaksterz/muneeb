package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;

/**
 * Created by freakster on 29/10/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true )
public class SupplierCredit extends BaseEntity{

    private String supplierId;
    private BigDecimal currCredit;
    private BigDecimal creditLimit;

    @Override
    public String toString() {
        return "SupplierCredit{" +
                "supplierId='" + supplierId + '\'' +
                ", currCredit=" + currCredit +
                ", creditLimit=" + creditLimit +
                '}';
    }
}
