package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;

/**
 * Created by freakster on 2/11/16.
 */
@Embedded
@Entity( noClassnameStored = true)
@Getter
@Setter
@ToString
public class DamageDetails {

    private String damageId;
    private int line;
    private String itemCode;
    private String lotName;
    private String lotNumber;
    private int quantity;
    private BigDecimal cost;
}
