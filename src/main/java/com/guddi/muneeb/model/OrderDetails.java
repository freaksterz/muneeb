package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Embedded;

import java.math.BigDecimal;

/**
 * Created by freakster on 30/10/16.
 */
@Setter
@Getter
@ToString
@Embedded
public class OrderDetails {

    private String orderId;
    private int line;
    private String itemCode;
    private String itemName;
    private int quantity;
    private BigDecimal unitPrice;
    private BigDecimal itemsPrice;
    private String siteCode;
}

