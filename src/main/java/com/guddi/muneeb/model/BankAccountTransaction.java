package com.guddi.muneeb.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by freakster on 1/11/16.
 */

@Embedded
@Entity(noClassnameStored = true)
@Getter
@Setter
@ToString
public class BankAccountTransaction {

    private String transactionId;
    private BigDecimal amount;
    private String type;    // debit or credit
    private Date transDate;
    private String to;
    private String from;


  /*  @Override
    public String toString() {
        return "BankAccount{" +
                "transactionId='" + transactionId + '\'' +
                ", amount='" + amount + '\'' +
                ", type='" + type + '\'' +
                ", transDate=" + transDate +
                ", to='" + to + '\'' +
                ", from='" + from + '\'' +

                '}';
    }
*/
}
