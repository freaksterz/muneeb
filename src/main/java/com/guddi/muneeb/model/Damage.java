package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.deser.ValueInstantiators;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.util.Date;

/**
 * Created by freakster on 2/11/16.
 */
@Getter
@Setter
@Entity(noClassnameStored = true)
public class Damage extends BaseEntity {

    private String damageId;
    private Date date;

    @Embedded
    DamageDetails damageDetails;

    @Override
    public String toString() {
        return "Damage{" +
                "damageId='" + damageId + '\'' +
                ", date=" + date +
                ", damageDetails=" + damageDetails +
                '}';
    }
}
