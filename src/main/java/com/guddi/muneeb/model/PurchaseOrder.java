package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by freakster on 29/10/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true )
public class PurchaseOrder extends BaseEntity{

    private String purOrderId;
    private Date poDate;
    private String supplierId;
    private String supplierName;
    private BigDecimal transportation;
    private BigDecimal discount;
    private BigDecimal totalAmount;
    private String paymentMode;  //TODO : convert to ENUM cash or credit
    private String lotNumber;
    private long lotName;
    private BigDecimal profitByOrder;

    @Embedded
    private OrderDetails orderDetails;

    @Override
    public String toString() {
        return "PurchaseOrder{" +
                "purOrderId='" + purOrderId + '\'' +
                ", poDate=" + poDate +
                ", supplierId='" + supplierId + '\'' +
                ", transportation=" + transportation +
                ", discount=" + discount +
                ", totalAmount=" + totalAmount +
                ", paymentMode='" + paymentMode + '\'' +
                ", lotNumber='" + lotNumber + '\'' +
                ", lotName=" + lotName +
                ", profitByOrder=" + profitByOrder +
                ", orderDetails=" + orderDetails +
                '}';
    }
}
