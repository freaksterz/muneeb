package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Embedded;

/**
 * Created by freaksterz on 27/08/16.
 * This domain class for storing the Address
 */
@Embedded
@Getter
@Setter
@ToString
public class Address {

    private String line1;
    private String line2;
    private String town;
    private String district;
    private String state;
    private String country;
    private String postcode;
    private long phoneNumber;

}