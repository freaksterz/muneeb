package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by freakster on 27/10/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true )
public class Item extends BaseEntity {

    private String itemCode;
    private String itemName;
    private String itemType;

    private int unitOfMeasure;

    @Override
    public String toString() {
        return "Item{" +
                "itemCode='" + itemCode + '\'' +
                ", itemName='" + itemName + '\'' +
                ", itemType='" + itemType + '\'' +
                ", unitOfMeasure=" + unitOfMeasure +
                '}';
    }
}
