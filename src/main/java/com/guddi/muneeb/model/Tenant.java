package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by freaksterz on 11/7/17
 */
@Setter
@Getter
@ToString
@Entity( noClassnameStored = true )
public class Tenant extends BaseEntity {

    private String tenantId;     //tenantId should consist of 3 alphabet of city, followed by int
    private String name;
    private String companyName;
    private String city;
    private String region;         // should be enum to be used later for analytics

    private boolean isActive;      // there should another class for the billing of tenant

    @Embedded
    private Address address;


}
