package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;

/**
 * Created by freakster on 29/10/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true)
public class CustomerCredit extends BaseEntity {

        private String customerId;
        private BigDecimal currCredit;
        private BigDecimal creditLimit;

    @Override
    public String toString() {
        return "CustomerCredit{" +
                "customerId='" + customerId + '\'' +
                ", currCredit=" + currCredit +
                ", creditLimit=" + creditLimit +
                '}';
    }
}
