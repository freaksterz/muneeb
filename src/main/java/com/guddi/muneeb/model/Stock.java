package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;

import java.util.Date;

/**
 * Created by freakster on 29/10/16.
 */
@Setter
@Getter
@Entity( noClassnameStored = true )
public class Stock extends BaseEntity {

    private String itemCode;
    private String itemName;
    private String quantity;
    private String siteCode;
    private String lotNo;
    private Date dateOfArrival;
    private String purOrderId;

    @Override
    public String toString() {
        return "Stock{" +
                "itemCode='" + itemCode + '\'' +
                ", itemName='" + itemName + '\'' +
                ", quantity='" + quantity + '\'' +
                ", siteCode='" + siteCode + '\'' +
                ", lotNo='" + lotNo + '\'' +
                ", dateOfArrival=" + dateOfArrival +
                ", purOrderId='" + purOrderId + '\'' +
                '}';
    }
}
