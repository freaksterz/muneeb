package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.math.BigInteger;
import java.util.Date;
/**
 * Created by rishu on 20/8/17.
 */
@ToString
@Setter
@Getter
@Entity( noClassnameStored = true)
public class PurchasePayment extends BaseEntity {


    private String paymentId;
    private String toPayment; // seller id
    private Date date;
    private boolean isGenericPayment;
    private boolean isOrderPayment;
    private String purchaseOrderId;
    private BigInteger amount;
    private String userId;
}
