package com.guddi.muneeb.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by rishu on 26/08/2017
 */

@Embedded
@Entity(noClassnameStored = true)
@Getter
@Setter
@ToString
public class BankTransaction {

    private String transactionId;
    private String userId;
    private String bankId;
    private BigDecimal amount;
    private String typeOfPayment;    // debit or credit
    private String modeOfPayment;    //cash /cheque/ online payment
    private String withdraw;
    private String deposit;
    private Date transDate;



  /*  @Override
    public String toString() {
        return "BankAccount{" +
                "transactionId='" + transactionId + '\'' +
                ", amount='" + amount + '\'' +
                ", type='" + type + '\'' +
                ", transDate=" + transDate +
                ", to='" + to + '\'' +
                ", from='" + from + '\'' +

                '}';
    }
*/
}
