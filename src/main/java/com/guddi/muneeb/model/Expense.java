package com.guddi.muneeb.model;

import lombok.Getter;
import lombok.Setter;
import org.mongodb.morphia.annotations.Entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by freakster on 1/11/16.
 */
@Getter
@Setter
@Entity ( noClassnameStored = true )
public class Expense extends BaseEntity {

    private String expenseId;
    private Date expenseDate;
    private String expenseType; // TODO: Change it to ENUM Business or home

    private BigDecimal amount;
    private String category;

    private String siteCode;
    private String comments;

    @Override
    public String toString() {
        return "Expense{" +
                "expenseId='" + expenseId + '\'' +
                ", expenseDate=" + expenseDate +
                ", expenseType='" + expenseType + '\'' +
                ", amount=" + amount +
                ", category='" + category + '\'' +
                ", siteCode='" + siteCode + '\'' +
                ", comments='" + comments + '\'' +
                '}';
    }
}
