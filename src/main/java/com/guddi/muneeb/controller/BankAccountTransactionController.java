package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.BankAccountTransaction;
import com.guddi.muneeb.services.BankAccountTransactionService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 11/7/17.
 */
@RestController
@Slf4j
public class BankAccountTransactionController {

    @Autowired
    private BankAccountTransactionService bankAccountTransactionService;

    @RequestMapping(value = "/bankaccountstransaction", method = RequestMethod.GET)
    public List<BankAccountTransaction> getBankAccountTransaction() {

        log.debug("entering getBankAccountTransaction");

        List<BankAccountTransaction> bankAccountTransaction = bankAccountTransactionService.getBankAccountTransaction();
        return bankAccountTransaction;

    }

    @RequestMapping(value = "/bankaccountstransaction", method = RequestMethod.POST)
    public ResponseEntity createBankAccountTransaction(@RequestBody BankAccountTransaction bankAccountTransaction) throws UnknownHostException {

        Key<BankAccountTransaction> baName = bankAccountTransactionService.createBankAccountTransaction(bankAccountTransaction);
        return new ResponseEntity<>(baName, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/bankaccountstransaction/count", method = RequestMethod.GET)
    public long getBankAccountTransactionsCount() {

        long bankAccountTransaction = bankAccountTransactionService.countBankAccountTransaction();
        return bankAccountTransaction;
    }

    @RequestMapping(value = "/bankaccountstransaction/{Id}", method = RequestMethod.GET)
    public BankAccountTransaction getBankAccountTransactionById(@RequestParam("id") String id) {

        BankAccountTransaction bankAccountTransaction = bankAccountTransactionService.getBankAccountTransaction(id);
        return bankAccountTransaction;

    }

    @RequestMapping(value = "/bankaccountstransaction", method = RequestMethod.PUT)
    public ResponseEntity<BankAccountTransaction> updateBankAccountTransaction(@RequestBody BankAccountTransaction bankAccountTransaction) throws UnknownHostException {

        BankAccountTransaction updatedBankAccountTransaction = bankAccountTransactionService.updateBankAccountTransaction(bankAccountTransaction);
        return new ResponseEntity<BankAccountTransaction>(updatedBankAccountTransaction, HttpStatus.OK);

    }

    @RequestMapping(value = "/bankaccountstransaction", method = RequestMethod.DELETE)
    public WriteResult deleteBankAccountTransactionById(@RequestParam("id") String id) {
        //customerService.deleteCustomerById(id);
        return bankAccountTransactionService.deleteBankAccountTransaction(id);

    }
}
