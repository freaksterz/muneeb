package com.guddi.muneeb.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by freaksterz on 11/9/17.
 */

@Controller
@Slf4j
public class OAuthController {

    @Autowired
    private TokenStore tokenStore;

    @RequestMapping(value = "oauth/logout", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpServletRequest httpServletRequest){

        String authHeader = httpServletRequest.getHeader("Authorization");

        if( authHeader!= null){
            //TODO : which user logout
            String tokenValue = authHeader.replace("Bearer","").trim();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
            tokenStore.removeAccessToken(accessToken);
        }

    }

}
