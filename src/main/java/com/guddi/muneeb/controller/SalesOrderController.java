package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.SalesOrder;
import com.guddi.muneeb.services.SalesOrderService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@RestController
@Slf4j
public class SalesOrderController {


    @Autowired
    private SalesOrderService salesOrderService;

    @RequestMapping(value = "/user/salesOrders", method = RequestMethod.GET)
    public List<SalesOrder> getSalesOrders(){

        log.debug("entering getSalesOrders");

        List<SalesOrder> salesOrders = salesOrderService.getSalesOrders();
        return salesOrders;

    }

    @RequestMapping(value = "/salesOrders/count", method = RequestMethod.GET)
    public long getSalesOrdersCount(){

        long salesOrdersCount = salesOrderService.countSalesOrders();
        return salesOrdersCount;

    }

    @RequestMapping(value = "/admin/salesOrder", method = RequestMethod.GET)
    public SalesOrder getSalesOrderById(@RequestParam("id") String id){

        SalesOrder salesOrder = salesOrderService.getSalesOrder(id);
        return salesOrder;

    }

    @RequestMapping(value = "/user/salesOrders/supplierId", method = RequestMethod.GET)
    public List<SalesOrder> getSalesOrderBySupplierId(@RequestParam("supplierId") String supplierId){

        List<SalesOrder> salesOrders = salesOrderService.getSalesOrderListByField("supplierId", supplierId);
        return  salesOrders;

    }


    @RequestMapping(value = "/user/salesOrders/supplierName", method = RequestMethod.GET)
    public List<SalesOrder> getSalesOrderBySalesOrderId(@RequestParam("supplierName") String supplierName){

        List<SalesOrder> salesOrders = salesOrderService.getSalesOrderListByField("supplierName", supplierName);
        return  salesOrders;

    }
}
