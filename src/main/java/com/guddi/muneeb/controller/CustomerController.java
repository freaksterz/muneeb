package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Customer;
import com.guddi.muneeb.services.CustomerService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;
import java.util.List;


/**
 * Created by freaksterz on 27/08/16.
 */
@RestController
@Slf4j
public class CustomerController {


    @Autowired
    private CustomerService customerService;


    @RequestMapping(value = "/user/customers", method = RequestMethod.GET)
    public List<Customer> getCustomers() {

        log.debug("entering getCustomers");

        List<Customer> customers = customerService.getCustomers();
        return customers;

    }


    @RequestMapping(value = "/user/customers/count", method = RequestMethod.GET)
    public long getCustomersCount() {

        long customersCount = customerService.countCustomers();
        return customersCount;

    }

    @RequestMapping(value = "/user/customer", method = RequestMethod.GET)
    public Customer getCustomerById(@RequestParam("id") String id) {

        Customer customer = customerService.getCustomer(id);
        return customer;

    }

    @RequestMapping(value = "/user/customer/phone", method = RequestMethod.GET)
    public Customer getCustomerByPhoneNumber(@RequestParam("phone") long phone) {

        Customer customer = customerService.getCustomerBy("phoneNumber", phone);
        return customer;

    }


    @RequestMapping(value = "/user/customer", method = RequestMethod.POST)
    public ResponseEntity createCustomer(@RequestBody Customer customer) throws UnknownHostException {

        Key<Customer> cName = customerService.createCustomer(customer);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/user/customer/{id}", method = RequestMethod.GET)
    public Customer getCustomer(@RequestParam("id") String id) {

        Customer customer = customerService.getCustomer(id);

        return customer;

    }

    @RequestMapping(value = "/user/customer", method = RequestMethod.PUT)
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) throws UnknownHostException {

        Customer customer1 = customerService.updateCustomer(customer);
        return new ResponseEntity<Customer>(customer1, HttpStatus.OK);

    }


    @RequestMapping(value = "/user/customer", method = RequestMethod.DELETE)
    public WriteResult deleteCustomerById(@RequestParam("id") String id) {

        //customerService.deleteCustomerById(id);
        return customerService.deleteCustomerById(id);

    }

}
