package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.SalesReturn;
import com.guddi.muneeb.services.SalesReturnService;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@RestController
@Slf4j
public class SalesReturnController {

    @Autowired
    private SalesReturnService salesReturnService;

    @RequestMapping(value = "/salesReturns", method = RequestMethod.GET)
    public List<SalesReturn> getSalesReturns(){

        log.debug("entering getSalesOrders");

        List<SalesReturn> salesReturns = salesReturnService.getSalesReturn();
        return salesReturns;

    }

    @RequestMapping(value = "/salesReturns/count", method = RequestMethod.GET)
    public long getSalesReturnsCount(){

        long salesReturnsCount = salesReturnService.countSalesReturn();
        return salesReturnsCount;

    }

    @RequestMapping(value = "/salesReturns/{id}", method = RequestMethod.GET)
    public SalesReturn getSalesReturnById(@RequestParam("id") String id){

        SalesReturn salesReturns = salesReturnService.getSalesReturn(id);
        return salesReturns;

    }

    /*@RequestMapping(value = "/user/salesOrders/supplierId", method = RequestMethod.GET)
    public List<SalesReturn> getSalesReturnBySupplierId(@RequestParam("supplierId") String supplierId){

        List<SalesReturn> salesOrders = salesReturnService.getSalesOrderListByField("supplierId", supplierId);
        return  salesOrders;

    }


    @RequestMapping(value = "/user/salesOrders/supplierName", method = RequestMethod.GET)
    public List<SalesReturn> getSalesOrderBySalesOrderId(@RequestParam("supplierName") String supplierName){

        List<SalesReturn> salesOrders = salesReturnService.getSalesOrderListByField("supplierName", supplierName);
        return  salesOrders;

    }*/

    @RequestMapping(value="/salesReturns", method= RequestMethod.POST)
    public ResponseEntity createSalesReturn(@RequestBody SalesReturn salesReturn) throws UnknownHostException {

        Key<SalesReturn> salesReturnName = salesReturnService.createSalesReturn(salesReturn);
        return new ResponseEntity<>(salesReturnName, HttpStatus.CREATED);

    }



    @RequestMapping(value="/salesReturns", method= RequestMethod.PUT)
    public ResponseEntity<SalesReturn> updateSalesReturn(@RequestBody SalesReturn salesReturn) throws UnknownHostException {

        SalesReturn salesReturn1 = salesReturnService.updateSalesReturn(salesReturn);
        return new ResponseEntity<SalesReturn>(salesReturn1, HttpStatus.OK);

    }

}
