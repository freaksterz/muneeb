package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.StockSite;
import com.guddi.muneeb.services.StockSiteService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by freakster on 3/11/16.
 */

@RestController
@Slf4j
public class StockSiteController {

    @Autowired
    private StockSiteService stockSiteService;


    @RequestMapping(value = "/user/stocksites", method = RequestMethod.GET)
    public List<StockSite> getStockSites() {
        log.debug("entering getStockSites");

        List<StockSite> stockSites = stockSiteService.getStockSites();
        return stockSites;

    }

    @RequestMapping(value = "/admin/stocksite", method = RequestMethod.GET)
    public StockSite getCustomerById(@RequestParam("siteCode") String siteCode) {

        StockSite stockSite = stockSiteService.getStockSiteBy("siteCode", siteCode);
        return stockSite;

    }

    @RequestMapping(value = "/user/stocksite", method = RequestMethod.POST)
    public ResponseEntity createStockSite(@RequestBody StockSite stockSite) throws UnknownHostException {
        Key<StockSite> stockSiteKey = stockSiteService.createStockSite(stockSite);
        return new ResponseEntity(stockSiteKey, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/user/stocksite", method = RequestMethod.PUT)
    public ResponseEntity updatetockSite(@RequestBody StockSite stockSite) throws UnknownHostException {
        Key<StockSite> stockSiteKey = stockSiteService.updateStockSite(stockSite);
        return new ResponseEntity(stockSiteKey, HttpStatus.OK);
    }

    @RequestMapping(value = "admin/stocksite", method = RequestMethod.DELETE)
    public WriteResult deleteCustomerById(@RequestParam("id") String id) {


        return stockSiteService.deleteStockSiteby("siteCode", id);

    }


}
