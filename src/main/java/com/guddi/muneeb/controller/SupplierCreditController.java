package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.SupplierCredit;
import com.guddi.muneeb.services.SupplierCreditService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */

@RestController
@Slf4j
public class SupplierCreditController {

    @Autowired
    private SupplierCreditService supplierCreditService;

    @RequestMapping(value = "/suppliercredits", method = RequestMethod.GET)
    public List<SupplierCredit> getSuppliersCredit(){

        log.debug("entering getSuppliers");

        List<SupplierCredit> suppliersCredit = supplierCreditService.getSupplierCredits();
        return suppliersCredit;

    }


    @RequestMapping(value = "/suppliercredits/count", method = RequestMethod.GET)
    public long getSuppliersCount(){

        long suppliersCreditCount = supplierCreditService.countSuppliersCredit();
        return suppliersCreditCount;

    }

    @RequestMapping(value = "/asuppliercredits", method = RequestMethod.GET)
    public SupplierCredit getSupplierCreditById(@RequestParam("id") String id){

        SupplierCredit supplierCredit = supplierCreditService.getSupplierCredit(id);
        return supplierCredit;

    }

    @RequestMapping(value="/suppliercredits", method= RequestMethod.POST)
    public ResponseEntity createSupplier(@RequestBody SupplierCredit supplierCredit) throws UnknownHostException {

        Key<SupplierCredit> cName = supplierCreditService.createSupplierCredit(supplierCredit);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }

    @RequestMapping(value="/suppliercredits/{id}", method = RequestMethod.GET)
    public SupplierCredit getSupplierCredit(@RequestParam("id") String id){

        SupplierCredit supplierCredit = supplierCreditService.getSupplierCredit(id);

        return supplierCredit;

    }

    @RequestMapping(value="/suppliercredits", method= RequestMethod.PUT)
    public ResponseEntity<SupplierCredit> updateSupplierCredit(@RequestBody SupplierCredit supplierCredit) throws UnknownHostException {

        SupplierCredit supplierCredit1 = supplierCreditService.updateSupplierCredit(supplierCredit);
        return new ResponseEntity<SupplierCredit>(supplierCredit1, HttpStatus.OK);

    }



    @RequestMapping(value="/suppliercredits", method = RequestMethod.DELETE)
    public WriteResult deleteSupplierCreditById(@RequestParam("id") String id){

        return  supplierCreditService.deleteSupplierCreditById(id);

    }

}
