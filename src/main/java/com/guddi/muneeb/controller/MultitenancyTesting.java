package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Customer;
import com.guddi.muneeb.services.CustomerService;
import com.guddi.muneeb.services.MultitenantService;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by aman on 19/8/17.
 */
@RestController
public class MultitenancyTesting {

    @Resource
    MultitenantService multitenantService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/mutitest", method = RequestMethod.POST)
    public ResponseEntity createCustomerMutitenantTest(@RequestBody Customer customer) throws UnknownHostException {

        Key<Customer> cName = multitenantService.createCustomerMulti(customer);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }

}
