package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Item;
import com.guddi.muneeb.services.ItemService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by freakster on 2/11/16.
 */

@RestController
@Slf4j
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/user/items", method = RequestMethod.GET)
    public List<Item> getItems() {

        log.debug("Entering getItems ");
        return itemService.getItems();

    }

    @RequestMapping(value = "/admin/item", method = RequestMethod.POST)
    public Key<Item> createItem(@RequestBody Item item) throws UnknownHostException {

        log.debug("Entering createItem ");
        return itemService.createItem(item);

    }

    @RequestMapping(value = "/admin/item", method = RequestMethod.DELETE)
    public WriteResult deleteItemByItemCode(@RequestParam("itemCode") String itemCode) {

        return itemService.deleteItemByCode(itemCode);
    }


}
