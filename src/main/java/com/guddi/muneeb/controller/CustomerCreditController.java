package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.CustomerCredit;
import com.guddi.muneeb.services.CustomerCreditService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;
import java.util.List;


/**
 * Created by freaksterz on 27/08/16.
 */
@RestController
@Slf4j
public class CustomerCreditController {

    @Autowired
    private CustomerCreditService customerCreditService;

    @RequestMapping(value = "/customerscredit", method = RequestMethod.GET)
    public List<CustomerCredit> getCustomersCreditCost() {

        log.debug("entering getCustomersCreditCost");
        List<CustomerCredit> customersCredit = customerCreditService.getCustomersCredit();
        return customersCredit;
    }

    @RequestMapping(value = "/customerscreditcount", method = RequestMethod.GET)
    public long getCustomerCreditCount() {

        long customersCount = customerCreditService.countCustomersCredit();
        return customersCount;

    }

    @RequestMapping(value = "/customerscreditbyid", method = RequestMethod.GET)
    public CustomerCredit getCustomerCreditById(@RequestParam("id") String id) {

        CustomerCredit customerCredit = customerCreditService.getCustomerCredit(id);
        return customerCredit;

    }

    @RequestMapping(value = "/customerscredit", method = RequestMethod.POST)
    public ResponseEntity createCustomerCredit(@RequestBody CustomerCredit customer) throws UnknownHostException {

        Key<CustomerCredit> cName = customerCreditService.createCustomerCredit(customer);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/customerscredit/{id}", method = RequestMethod.GET)
    public CustomerCredit getCustomerCredit(@RequestParam("id") String id) {

        CustomerCredit customerCredit = customerCreditService.getCustomerCredit(id);
        return customerCredit;
    }

    @RequestMapping(value = "/customerscredit", method = RequestMethod.PUT)
    public ResponseEntity<CustomerCredit> updateCustomerCredit(@RequestBody CustomerCredit customerCredit) throws UnknownHostException {

        CustomerCredit customerCredit1 = customerCreditService.updateCustomerCredit(customerCredit);
        return new ResponseEntity<CustomerCredit>(customerCredit1, HttpStatus.OK);

    }

    @RequestMapping(value = "/customerscredit", method = RequestMethod.DELETE)
    public WriteResult deleteCustomerCreditById(@RequestParam("id") String id) {

        //customerService.deleteCustomerById(id);
        return customerCreditService.deleteCustomerCreditById(id);

    }

}
