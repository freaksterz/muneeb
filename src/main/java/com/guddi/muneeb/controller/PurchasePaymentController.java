package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.PurchasePayment;
import com.guddi.muneeb.services.PurchasePaymentService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 20/8/17.
 */
@RestController
@Slf4j
public class PurchasePaymentController {

    @Autowired
    private PurchasePaymentService purchasePaymentService;

    @RequestMapping(value = "/purchasePayment", method = RequestMethod.GET)
    public List<PurchasePayment> getPurchasePayment(){
        log.debug("entering salesPayment id");
        List<PurchasePayment> purchasePayment = purchasePaymentService.getPurchasePayment();
        return purchasePayment;
    }

    @RequestMapping(value="/purchasePayment", method= RequestMethod.POST)
    public ResponseEntity createPurchasePayment(@RequestBody PurchasePayment purchasePayment) throws UnknownHostException {
        Key<PurchasePayment> baName = purchasePaymentService.createPurchasePayment(purchasePayment);
        return new ResponseEntity<>(baName, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/purchasePayment/count", method = RequestMethod.GET)
    public long getPurchasePaymentCount(){
        long purchasePaymentCount = purchasePaymentService.countPurchasePayment();
        return purchasePaymentCount;
    }

    @RequestMapping(value = "/purchasePayment/{Id}", method = RequestMethod.GET)
    public PurchasePayment getPurchasePaymentId(@RequestParam("id") String id){
        PurchasePayment purchasePayment = purchasePaymentService.getPurchasePayment(id);
        return purchasePayment;
    }

    @RequestMapping(value="/purchasePayment", method= RequestMethod.PUT)
    public ResponseEntity<PurchasePayment> updatePurchasePayment(@RequestBody PurchasePayment purchasePayment) throws UnknownHostException {
        PurchasePayment updatedPurchasePayment = purchasePaymentService.updatePurchasePayment(purchasePayment);
        return new ResponseEntity<PurchasePayment>(updatedPurchasePayment, HttpStatus.OK);
    }

    @RequestMapping(value="/purchasePayment", method = RequestMethod.DELETE)
    public WriteResult deletePurchasePaymentById(@RequestParam("id") String id){

        return  purchasePaymentService.deletePurchasePaymentId(id);

    }
}
