package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.SalesPayment;
import com.guddi.muneeb.services.SalesPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;
/**
 * Created by rishu on 20/8/17.
 */
@RestController
@Slf4j
public class SalesPaymentController {

    @Autowired
    private SalesPaymentService salesPaymentService;

    @RequestMapping(value = "/salesPayment", method = RequestMethod.GET)
    public List<SalesPayment> getSalesPayment(){
        log.debug("entering salesPayment id");
        List<SalesPayment> salesPayment = salesPaymentService.getSalesPayment();
        return salesPayment;
    }

    @RequestMapping(value="/salesPayment", method= RequestMethod.POST)
    public ResponseEntity createSalesPayment(@RequestBody SalesPayment salesPayment) throws UnknownHostException {
        Key<SalesPayment> baName = salesPaymentService.createSalesPayment(salesPayment);
        return new ResponseEntity<>(baName, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/salesPayment/count", method = RequestMethod.GET)
    public long getSalesPaymentCount(){
        long salesPaymentCount = salesPaymentService.countSalesPayment();
        return salesPaymentCount;
    }

    @RequestMapping(value = "/salesPayment/{Id}", method = RequestMethod.GET)
    public SalesPayment getSalesPaymentId(@RequestParam("id") String id){
        SalesPayment salesPayment = salesPaymentService.getSalesPayment(id);
        return salesPayment;
    }

    @RequestMapping(value="/salesPayment", method= RequestMethod.PUT)
    public ResponseEntity<SalesPayment> updateSalesPayment(@RequestBody SalesPayment salesPayment) throws UnknownHostException {
        SalesPayment updatedSalesPayment = salesPaymentService.updateSalesPayment(salesPayment);
        return new ResponseEntity<SalesPayment>(updatedSalesPayment, HttpStatus.OK);
    }

    @RequestMapping(value="/salesPayment", method = RequestMethod.DELETE)
    public WriteResult deleteSalesPaymentById(@RequestParam("id") String id){

        return  salesPaymentService.deleteSalesPaymentId(id);

    }
}
