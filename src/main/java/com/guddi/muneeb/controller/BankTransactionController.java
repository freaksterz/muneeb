package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.BankTransaction;
import com.guddi.muneeb.services.BankTransactionService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 11/7/17.
 */
@RestController
@Slf4j
public class BankTransactionController {

    @Autowired
    private BankTransactionService bankTransactionService;

    @RequestMapping(value = "/bankTransaction", method = RequestMethod.GET)
    public List<BankTransaction> getBankTransaction() {
        log.debug("entering getBankTransaction");
        List<BankTransaction> bankTransaction = bankTransactionService.getBankTransaction();
        return bankTransaction;
    }

    @RequestMapping(value = "/bankTransaction", method = RequestMethod.POST)
    public ResponseEntity createBankTransaction(@RequestBody BankTransaction bankTransaction) throws UnknownHostException {

        Key<BankTransaction> baName = bankTransactionService.createBankTransaction(bankTransaction);
        return new ResponseEntity<>(baName, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/bankTransaction/count", method = RequestMethod.GET)
    public long getBankTransactionsCount() {

        long bankTransaction = bankTransactionService.countBankTransaction();
        return bankTransaction;
    }

    @RequestMapping(value = "/bankTransaction/{Id}", method = RequestMethod.GET)
    public BankTransaction getBankTransactionById(@RequestParam("id") String id) {

        BankTransaction bankTransaction = bankTransactionService.getBankTransaction(id);
        return bankTransaction;

    }

    @RequestMapping(value = "/bankTransaction", method = RequestMethod.PUT)
    public ResponseEntity<BankTransaction> updateBankTransaction(@RequestBody BankTransaction bankTransaction) throws UnknownHostException {

        BankTransaction updatedBankTransaction = bankTransactionService.updateBankTransaction(bankTransaction);
        return new ResponseEntity<BankTransaction>(updatedBankTransaction, HttpStatus.OK);

    }

    @RequestMapping(value = "/bankTransaction", method = RequestMethod.DELETE)
    public WriteResult deleteBankTransactionById(@RequestParam("id") String id) {
        //customerService.deleteCustomerById(id);
        return bankTransactionService.deleteBankTransaction(id);

    }
}
