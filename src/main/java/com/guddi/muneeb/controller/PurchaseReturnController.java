package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.PurchaseReturn;
import com.guddi.muneeb.services.PurchaseReturnService;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;
import java.util.List;
/**
 * Created by rishu on 13/7/17.
 */
@RestController
@Slf4j
public class PurchaseReturnController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PurchaseReturnService purchaseReturnService;

    @RequestMapping(value = "/purchaseReturns", method = RequestMethod.GET)
    public List<PurchaseReturn> getPurchaseReturns(){

        logger.debug("entering getPurchaseReturns");

        List<PurchaseReturn> purchaseOrders = purchaseReturnService.getPurchaseReturns();
        return purchaseOrders;

    }

    @RequestMapping(value = "/purchaseReturns/count", method = RequestMethod.GET)
    public long getPurchaseReturnsCount(){

        long purchaseOrdersCount = purchaseReturnService.countPurchaseReturn();
        return purchaseOrdersCount;

    }

    @RequestMapping(value = "/purchaseReturns/{id}", method = RequestMethod.GET)
    public PurchaseReturn getPurchaseReturnById(@RequestParam("id") String id){

        PurchaseReturn purchaseReturn = purchaseReturnService.getPurchaseReturnById(id);
        return purchaseReturn;

    }

    @RequestMapping(value="/purchaseReturns", method= RequestMethod.POST)
    public ResponseEntity createPurchaseReturn(@RequestBody PurchaseReturn purchaseReturn) throws UnknownHostException {

        Key<PurchaseReturn> cName = purchaseReturnService.createPurchaseReturn(purchaseReturn);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }


    @RequestMapping(value="/purchaseReturn", method= RequestMethod.PUT)
    public ResponseEntity<PurchaseReturn> updatePurchaseReturn(@RequestBody PurchaseReturn purchaseReturn) throws UnknownHostException {

        PurchaseReturn purchaseReturn1 = purchaseReturnService.updatePurchaseReturn(purchaseReturn);
        return new ResponseEntity<PurchaseReturn>(purchaseReturn1, HttpStatus.OK);

    }



}

