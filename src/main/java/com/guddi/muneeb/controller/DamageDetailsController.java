package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.DamageDetails;
import com.guddi.muneeb.services.DamageDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */
@RestController
@Slf4j
public class DamageDetailsController {

    @Autowired
    private DamageDetailsService damageDetailsService;

    /* @RequestMapping(value = "/damagesdetails", method = RequestMethod.GET)
     public List<DamageDetails> getDamageDetails() {

         log.debug("entering getDamagesDetails");
         List<DamageDetails> damagesDetails = damageDetailsService.damageDetails();
         return damagesDetails;
     }
 */
    @RequestMapping(value = "/damagesdetails", method = RequestMethod.GET)
    public long getDamageDetailsCount() {

        long damagesDetailsCount = damageDetailsService.countDamagesDetails();
        return damagesDetailsCount;

    }

    @RequestMapping(value = "/damagesdetails", method = RequestMethod.POST)
    public ResponseEntity createDamageDetails(@RequestBody DamageDetails damageDetails) throws UnknownHostException {

        Key<DamageDetails> cName = damageDetailsService.createDamageDetails(damageDetails);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/damagesdetails", method = RequestMethod.PUT)
    public ResponseEntity<DamageDetails> updateDamageDetails(@RequestBody DamageDetails damageDetails) throws UnknownHostException {

        DamageDetails damageDetails1 = damageDetailsService.updateDamageDetails(damageDetails);
        return new ResponseEntity<DamageDetails>(damageDetails1, HttpStatus.OK);

    }

    /*@RequestMapping(value = "/damagesdetails", method = RequestMethod.GET)
    public DamageDetails getDamageDetailsById(@RequestParam("id") String id) {

        DamageDetails damageDetails = damageDetailsService.damageDetailsById(id);
        return damageDetails;

    }*/

}

