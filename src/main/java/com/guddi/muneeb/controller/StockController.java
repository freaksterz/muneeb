package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Stock;
import com.guddi.muneeb.services.StockService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */
@RestController
@Slf4j
public class StockController {

    @Autowired
    private StockService stockService;


    @RequestMapping(value = "/stock", method = RequestMethod.GET)
    public List<Stock> getStocks() {
        log.debug("entering getStockSites");

        List<Stock> stock = stockService.getStocks();
        return stock;

    }

    @RequestMapping(value = "/stock/{id}", method = RequestMethod.GET)
    public Stock getStockById(@RequestParam("siteCode") String siteCode) {

        Stock stock = stockService.getStockBy("siteCode", siteCode);
        return stock;

    }

    @RequestMapping(value = "/stock", method = RequestMethod.POST)
    public ResponseEntity createStock(@RequestBody Stock stockSite) throws UnknownHostException {
        Key<Stock> stockKey = stockService.createStock(stockSite);
        return new ResponseEntity(stockKey, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/stock", method = RequestMethod.PUT)
    public ResponseEntity updateStock(@RequestBody Stock stock) throws UnknownHostException {
        Key<Stock> stockKey = stockService.updateStock(stock);
        return new ResponseEntity(stockKey, HttpStatus.OK);
    }

    @RequestMapping(value = "/stock", method = RequestMethod.DELETE)
    public WriteResult deleteStockById(@RequestParam("id") String id) {


        return stockService.deleteStockby("siteCode", id);
    }
}
