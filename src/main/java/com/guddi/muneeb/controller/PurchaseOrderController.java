package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.PurchaseOrder;
import com.guddi.muneeb.services.PurchaseOrderService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */
@RestController
@Slf4j
public class PurchaseOrderController {

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @RequestMapping(value = "/user/purchaseOrders", method = RequestMethod.GET)
    public List<PurchaseOrder> getPurchaseOrders(){

        log.debug("entering getPurchaseOrders");

        List<PurchaseOrder> purchaseOrders = purchaseOrderService.getPurchaseOrders();
        return purchaseOrders;

    }

    @RequestMapping(value = "/purchaseOrders/count", method = RequestMethod.GET)
    public long getPurchaseOrdersCount(){

        long purchaseOrdersCount = purchaseOrderService.countPurchaseOrders();
        return purchaseOrdersCount;

    }

    @RequestMapping(value = "/admin/purchaseOrder", method = RequestMethod.GET)
    public PurchaseOrder getPurchaseOrderById(@RequestParam("id") String id){

        PurchaseOrder purchaseOrder = purchaseOrderService.getPurchaseOrder(id);
        return purchaseOrder;

    }

    @RequestMapping(value = "/user/purchaseOrders/supplierId", method = RequestMethod.GET)
    public List<PurchaseOrder> getPurchaseOrderBySupplierId(@RequestParam("supplierId") String supplierId){

        List<PurchaseOrder> purchaseOrders = purchaseOrderService.getPurchaseOrderListByField("supplierId", supplierId);
        return  purchaseOrders;

    }


    @RequestMapping(value = "/user/purchaseOrders/supplierName", method = RequestMethod.GET)
    public List<PurchaseOrder> getPurchaseOrderByPurchaseOrderId(@RequestParam("supplierName") String supplierName){

        List<PurchaseOrder> purchaseOrders = purchaseOrderService.getPurchaseOrderListByField("supplierName", supplierName);
        return  purchaseOrders;

    }




}
