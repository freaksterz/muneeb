package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Tenant;
import com.guddi.muneeb.services.TenantService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by freaksterz on 11/7/17.
 */

@RestController
@Slf4j
public class TenantController {

    @Autowired
    private TenantService tenantService;

    @RequestMapping(value = "/tenant", method = RequestMethod.GET)
    public List<Tenant> getItems() {

        log.debug("Entering getItems ");
        return tenantService.getTenants();

    }

    @RequestMapping(value = "/tenant", method = RequestMethod.POST)
    public Key<Tenant> createItem(@RequestBody Tenant tenant) throws UnknownHostException {

        log.debug("Entering createItem ");
        return tenantService.createTenant(tenant);

    }

    @RequestMapping(value = "/tenant", method = RequestMethod.DELETE)
    public WriteResult deleteTenantByTenantId(@RequestParam("tenantId") String tenantId) {

        return tenantService.deleteTenantById(tenantId);
    }


}