package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Supplier;
import com.guddi.muneeb.services.SupplierService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;


/**
 * Created by freaksterz on 27/08/16.
 */

@RestController
@Slf4j
public class SupplierController {


    @Resource(name="supplierService")
    private SupplierService supplierService;

    @RequestMapping(value = "/user/suppliers", method = RequestMethod.GET)
    public List<Supplier> getSuppliers(){

        log.debug("entering getSuppliers");

        List<Supplier> suppliers = supplierService.getSuppliers();
        return suppliers;

    }


    @RequestMapping(value = "/suppliers/count", method = RequestMethod.GET)
    public long getSuppliersCount(){

        long suppliersCount = supplierService.countSuppliers();
        return suppliersCount;

    }

    @RequestMapping(value = "/admin/supplier", method = RequestMethod.GET)
    public Supplier getSupplierById(@RequestParam("id") String id){

        Supplier supplier = supplierService.getSupplier(id);
        return supplier;

    }

    @RequestMapping(value = "/user/supplier/phone", method = RequestMethod.GET)
    public Supplier getSupplierByPhoneNumber(@RequestParam("phone") long phone){

        Supplier supplier = supplierService.getSupplierBy("phoneNumber", phone);
        return supplier;

    }



    @RequestMapping(value="/admin/supplier", method= RequestMethod.POST)
    public ResponseEntity createSupplier(@RequestBody Supplier supplier) throws UnknownHostException {

        Key<Supplier> cName = supplierService.createSupplier(supplier);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }

    @RequestMapping(value="/user/supplier/{id}", method = RequestMethod.GET)
    public Supplier getSupplier(@RequestParam("id") String id){

        Supplier supplier = supplierService.getSupplier(id);

        return supplier;

    }

    @RequestMapping(value="/admin/supplier", method= RequestMethod.PUT)
    public ResponseEntity<Supplier> updateSupplier(@RequestBody Supplier supplier) throws UnknownHostException {

        Supplier supplier1 = supplierService.updateSupplier(supplier);
        return new ResponseEntity<Supplier>(supplier1, HttpStatus.OK);

    }



    @RequestMapping(value="admin/supplier", method = RequestMethod.DELETE)
    public WriteResult deleteSupplierById(@RequestParam("id") String id){

        return  supplierService.deleteSupplierById(id);

    }

}
