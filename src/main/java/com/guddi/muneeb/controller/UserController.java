package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.User;
import com.guddi.muneeb.services.SecUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by freakster on 9/10/16.
 */
@RestController
@Slf4j
public class UserController {

    @Autowired
    private SecUserDetailsService secUserDetailsService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> getUsers() {
        List<User> userList = secUserDetailsService.getUsers();
        return userList;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity createUser(@RequestBody User user) throws UnknownHostException {

        Key<User> userKey = secUserDetailsService.createUser(user);
        return new ResponseEntity<>(userKey, HttpStatus.CREATED);
    }

}