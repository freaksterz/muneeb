package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.BankAccount;
import com.guddi.muneeb.services.BankAccountService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 11/7/17.
 */
@RestController
@Slf4j
public class BankAccountController {


    @Autowired
    private BankAccountService bankAccountService;

    @RequestMapping(value = "/bankaccount", method = RequestMethod.GET)
    public List<BankAccount> getBankAccount() {

        log.debug("entering getBankAccount");

        List<BankAccount> bankAccount = bankAccountService.getBankAccount();
        return bankAccount;

    }

    @RequestMapping(value = "/bankaccount", method = RequestMethod.POST)
    public ResponseEntity createBankAccount(@RequestBody BankAccount bankAccount) throws UnknownHostException {

        Key<BankAccount> baName = bankAccountService.createBankAccount(bankAccount);
        return new ResponseEntity<>(baName, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/bankaccount/count", method = RequestMethod.GET)
    public long getCustomersCount() {

        long bankAccountCount = bankAccountService.countBankAccount();
        return bankAccountCount;
    }

    @RequestMapping(value = "/bankaccount/{Id}", method = RequestMethod.GET)
    public BankAccount getBankAccountById(@RequestParam("id") String id) {

        BankAccount bankAccount = bankAccountService.getBankAccount(id);
        return bankAccount;

    }

    @RequestMapping(value = "/bankaccount", method = RequestMethod.PUT)
    public ResponseEntity<BankAccount> updateBankAccount(@RequestBody BankAccount bankAccount) throws UnknownHostException {

        BankAccount updatedBankAccount = bankAccountService.updateBankAccount(bankAccount);
        return new ResponseEntity<BankAccount>(updatedBankAccount, HttpStatus.OK);

    }

    @RequestMapping(value = "/bankaccount", method = RequestMethod.DELETE)
    public WriteResult deleteBankAccountById(@RequestParam("id") String id) {

        //customerService.deleteCustomerById(id);
        return bankAccountService.deleteBankAccountId(id);

    }
}
