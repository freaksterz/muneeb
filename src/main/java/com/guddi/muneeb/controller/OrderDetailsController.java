package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.OrderDetails;
import com.guddi.muneeb.services.OrderDetailsService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by rishu on 12/7/17.
 */
@RestController
@Slf4j
public class OrderDetailsController {

    @Autowired
    private OrderDetailsService orderDetailsService;

    @RequestMapping(value = "/orderdetails", method = RequestMethod.GET)
    public List<OrderDetails> getOrdersDetails() {

        log.debug("entering OrdersDetails");

        List<OrderDetails> orderDetails = orderDetailsService.getOrderDetails();
        return orderDetails;

    }

    @RequestMapping(value = "/orderdetails/count", method = RequestMethod.GET)
    public long getOrdersDetailsCount() {

        long orderDetailsCount = orderDetailsService.countOrderDetails();
        return orderDetailsCount;

    }

    @RequestMapping(value = "/orderdetails/{id}", method = RequestMethod.GET)
    public OrderDetails getOrdersDetailsById(@RequestParam("id") String id) {

        OrderDetails orderDetails = orderDetailsService.getOrderDetails(id);
        return orderDetails;
    }

    @RequestMapping(value = "/orderdetails", method = RequestMethod.POST)
    public ResponseEntity createOrdersDetails(@RequestBody OrderDetails orderDetails) throws UnknownHostException {

        Key<OrderDetails> orderDetails1 = orderDetailsService.createOrderDetails(orderDetails);
        return new ResponseEntity<>(orderDetails1, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/orderdetails", method = RequestMethod.PUT)
    public ResponseEntity<OrderDetails> updateOrdersDetails(@RequestBody OrderDetails orderDetails) throws UnknownHostException {

        OrderDetails orderDetails1 = orderDetailsService.updateOrderDetails(orderDetails);
        return new ResponseEntity<OrderDetails>(orderDetails1, HttpStatus.OK);

    }

    @RequestMapping(value = "/orderdetails", method = RequestMethod.DELETE)
    public WriteResult deleteOrdersDetails(@RequestParam("id") String id) {

        return orderDetailsService.deleteOrderDetailsById(id);
    }
}
