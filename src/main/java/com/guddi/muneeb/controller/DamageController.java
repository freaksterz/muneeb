package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Damage;
import com.guddi.muneeb.services.DamageService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by freakster on 9/11/16.
 */
@RestController
@Slf4j
public class DamageController {

    @Autowired
    private DamageService damageService;

    @RequestMapping(value = "/user/damages", method = RequestMethod.GET)
    public List<Damage> getDamages() {

        log.debug("entering getDamages");

        List<Damage> damages = damageService.getDamages();
        return damages;

    }


    @RequestMapping(value = "/damages/count", method = RequestMethod.GET)
    public long getDamagesCount() {

        long damagesCount = damageService.countDamages();
        return damagesCount;

    }

    @RequestMapping(value = "/admin/damage", method = RequestMethod.GET)
    public Damage getDamageById(@RequestParam("id") String id) {

        Damage damage = damageService.getDamage(id);
        return damage;

    }

}
