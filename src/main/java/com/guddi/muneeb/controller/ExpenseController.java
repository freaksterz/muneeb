package com.guddi.muneeb.controller;

import com.guddi.muneeb.model.Expense;
import com.guddi.muneeb.services.ExpenseService;
import com.mongodb.WriteResult;
import lombok.extern.slf4j.Slf4j;
import org.mongodb.morphia.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by freakster on 8/11/16.
 */

@RestController
@Slf4j
public class ExpenseController {

    @Autowired
    private ExpenseService expenseService;

    @RequestMapping(value = "/user/expenses", method = RequestMethod.GET)
    public List<Expense> getExpenses() {

        log.debug("entering getExpenses");

        List<Expense> expenses = expenseService.getExpenses();
        return expenses;

    }


    @RequestMapping(value = "/expenses/count", method = RequestMethod.GET)
    public long getExpensesCount() {

        long expensesCount = expenseService.countExpenses();
        return expensesCount;

    }

    @RequestMapping(value = "/admin/expense", method = RequestMethod.GET)
    public Expense getExpenseById(@RequestParam("id") String id) {

        Expense expense = expenseService.getExpense(id);
        return expense;

    }

    @RequestMapping(value = "/user/expense/phone", method = RequestMethod.GET)
    public Expense getExpenseByPhoneNumber(@RequestParam("phone") long phone) {

        Expense expense = expenseService.getExpenseBy("phoneNumber", phone);
        return expense;

    }


    @RequestMapping(value = "/admin/expense", method = RequestMethod.POST)
    public ResponseEntity createExpense(@RequestBody Expense expense) throws UnknownHostException {

        Key<Expense> cName = expenseService.createExpense(expense);
        return new ResponseEntity<>(cName, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/user/expense/{id}", method = RequestMethod.GET)
    public Expense getExpense(@RequestParam("id") String id) {

        Expense expense = expenseService.getExpense(id);

        return expense;

    }

    @RequestMapping(value = "/admin/expense", method = RequestMethod.PUT)
    public ResponseEntity<Expense> updateExpense(@RequestBody Expense expense) throws UnknownHostException {

        Expense expense1 = expenseService.updateExpense(expense);
        return new ResponseEntity<Expense>(expense1, HttpStatus.OK);

    }


    @RequestMapping(value = "admin/expense", method = RequestMethod.DELETE)
    public WriteResult deleteExpenseById(@RequestParam("id") String id) {

        //expenseService.deleteExpenseById(id);
        return expenseService.deleteExpenseById(id);

    }


}
